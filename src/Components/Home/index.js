import React from 'react';

import OrdersList from '../Orders/OrdersList';

const Home = () => (
  <div>
    <OrdersList />
  </div>
);

export default Home;
