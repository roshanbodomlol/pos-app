import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import Home from '../Home';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  main: {
    padding: theme.spacing.unit * 3,
    flexGrow: 1
  }
});

const Main = ({ classes }) => (
  <div className={classes.main}>
    <div className={classes.appBarSpacer} />
    <Switch>
      <Route path="/" component={Home} />
      <Redirect to="/" />
    </Switch>
  </div>
);

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Main);
